﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace CustomFailText
{
    class FailTextRandomizer : MonoBehaviour
    {
        public static FailTextRandomizer instance;
        public TextMeshPro targetText;
        public GameEnergyCounter energyCounter;
        public bool latch;

        public void Awake()
        {
            if (instance != this)
            {
                if (instance != null) Destroy(instance.gameObject);
                instance = this;
            }
            targetText = GameObject.Find("LevelFailedTextEffect")?.GetComponent<TextMeshPro>();
            energyCounter = GameObject.Find("GameEnergyCounter")?.GetComponent<GameEnergyCounter>();
        }

        public void LateUpdate()
        {
            if (targetText == null || energyCounter == null)
            {
                Awake(); // look for our resources again
            }
            if (targetText == null || energyCounter == null)
            {
                return; // they're not here, keep waiting I guess
            }

            if (energyCounter.energy < 1E-05f)
            {
                if (latch) return; // don't randomize again until energy goes above 0
                latch = true;

                if (Plugin.allEntries.Count == 0)
                {
                    setText(Plugin.DEFAULT_TEXT);
                }
                else
                {
                    // Choose an entry randomly

                    // Unity's random seems to give biased results
                    // int entryPicked = UnityEngine.Random.Range(0, entriesInFile.Count);
                    // using System.Random instead
                    System.Random r = new System.Random();
                    int entryPicked = r.Next(Plugin.allEntries.Count);

                    // Set the text
                    setText(Plugin.allEntries[entryPicked]);
                }
            }
            else
            {
                latch = false; // allow randomization again
            }
        }

        private void setText(string[] lines)
        {

            // Prevent undesired word wrap
            targetText.overflowMode = TextOverflowModes.Overflow;
            targetText.enableWordWrapping = false;

            // Set the text
            targetText.text = String.Join("\n", lines);
        }
    }
}
